//Criando um Repository
const Modelo = require('./modeloTabelaFornecedor');

module.exports = {

    listar() {
        return Modelo.findAll();
    },
    inserir(fornecedor) {
        return Modelo.create(fornecedor)
    },
    async pegarPorId(id){
        const result =  await Modelo.findOne({
            where: {
                id: id
            }
        })

        if (!result) {
            throw new Error('Fornecedor não Encontrado!')
        }

        return result;
    },
    atualizar(id, dadosParaAtualizar) {
        return Modelo.update(
            dadosParaAtualizar,
            {
                where: {
                    id: id
                }
            }
            
        )
    }
}